import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
// import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import LandingPage from "./pages/landingpage/Index";
import Search from "./pages/searchcar/Search";
import { Provider } from "react-redux";
import store from "./redux/store";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route path="/cars" element={<Search />} />
          </Routes>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
