import React from 'react';
import Navbar from '../../components/landingpage/navbar/Navbar';
import Jumbotron from '../../components/landingpage/jumbotron/Jumbotron';
import OurServices from '../../components/landingpage/ourservices/OurServices';
import WhyUs from '../../components/landingpage/whyus/WhyUs';
import Testimonial from '../../components/landingpage/testimonial/Testimonial';
import CtaBanner from '../../components/landingpage/ctabanner/CtaBanner';
import Faq from '../../components/landingpage/faq/Faq';
import Footer from '../../components/landingpage/footer/Footer';
import { useEffect } from "react";

const LandingPage = () => {
  useEffect(() => {
    document.title = "Homepage";
  }, []);


  return (
    <>
      {/* START NAVBAR */}
      <Navbar />
      {/* END NAVBAR */}
      {/* START HERO DESCRIPTION */}
      <Jumbotron />
      {/* END HERO DESCRIPTION */}
      {/* START OUR SERVICES */}
      <OurServices />
      {/* END OUR SERVICES */}
      {/* START WHY US */}
      <WhyUs />
      {/* END WHY US */}
      {/* START TESTIMONIAL */}

      {<Testimonial />}
      {/* END TESTIMONIAL */}
      {/* START CTA BANNER */}
      <CtaBanner />
      {/* END CTA BANNER */}
      {/* START FAQ */}
      <Faq />
      {/* END FAQ */}
      {/* START FOOTER */}
      <Footer />
      {/* END FOOTER */}
    </>
  );
};

export default LandingPage;
