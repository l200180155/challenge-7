import { useEffect } from "react";
import Filter from "../../components/searchcar/carfilter/CarFilter";
import Footer from "../../components/searchcar/footer/Footer";
import Jumbotron from '../../components/searchcar/jumbotron/Jumbotron';
import Navbar from "../../components/searchcar/navbar/Navbar";

const Search = () => {
  useEffect(() => {
    document.title = "Search Cars";
  }, []);
  return (
    <>
      <Navbar />
      <Jumbotron />
      <Filter />
        <Footer />
    </>
  );
};

export default Search;
