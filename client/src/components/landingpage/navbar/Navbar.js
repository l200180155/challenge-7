import { Link } from "react-router-dom";
import './Navbar.css'

const Navbar = () => {
  return (
    <>
    <div className="container-fluid">
        <div className="wrap-landing-page">
            {/* <!-- START OF NAVIGATION BAR --> */}
            <nav className="navbar navbar-expand-lg navbar-light pt-5">
                {/* <!-- MENGUBAH NAVBAR MENJADI TERANG BERTULISKAN GELAP DENGAN PADDING TOP DAN BOTTOM 5 --> */}
                <Link className="navbar-brand text-light" to="/">Iqbal</Link>
                {/* <!-- MENGUBAH WARNA TULISAN BRAND JADI PUTIH --> */}
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-Nav" aria-controls="navbar-Nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbar-Nav">
                    <ul className="navbar-nav ms-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="#our-service">Our Services</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#why-us">Why Us</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#testimonial">Testimonial</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#faq">FAQ</a>
                        </li>
                        <li className="nav-item">
                            <button className="btn btn-success" type="submit">Register</button>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    </>
  );
};

export default Navbar;