import React from "react";
import './WhyUs.css'

const WhyUs = () => {
  return (
    <div className="container-fluid">
        <div className="margin-content">
            <div className="why-us" id="why-us">
                  <h3>Why Us?</h3>
                  <p>Mengapa harus pilih Binar Car Rental?</p>
                  <div className="row justify-content-between mt-5">
                      <div className="card">
                          <img className="card-img-top" src="img/icon-complete.png" alt=""/>
                          <div className="card-body">
                              <h5 className="card-title">Mobil Lengkap</h5>
                              <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                              </p>
                          </div>
                      </div>
                      <div className="card">
                          <img className="card-img-top" src="img/icon-price.png" alt=""/>
                          <div className="card-body">
                              <h5 className="card-title">Harga Murah</h5>
                              <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain
                              </p>
                          </div>
                      </div>
                      <div className="card">
                          <img className="card-img-top" src="img/icon-24hrs.png" alt=""/>
                          <div className="card-body">
                              <h5 className="card-title">Layanan 24 Jam</h5>
                              <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu
                              </p>
                          </div>
                      </div>
                      <div className="card">
                          <img className="card-img-top" src="img/icon-professional.png" alt=""/>
                          <div className="card-body">
                              <h5 className="card-title">Sopir Profesional</h5>
                              <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu
                              </p>
                          </div>
                      </div>
                  </div>
              </div>
        </div>
    </div>
  );
};

export default WhyUs;
