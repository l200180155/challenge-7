import './OurServices.css'

const OurServices = () => {
  return (
    <div className="container-fluid">
      <div className="margin-content">
            {/* <!-- START OF OUR SERVICES --> */}
            <div className="our-service mx-auto" id="our-service">
                <div className="row">
                    <div className="col-lg-6">
                        <img className="img-service" src="img/img-service.png" alt=""/>
                    </div>
                    <div className="col-lg-6">
                        <h3>Best Car Rental for any kind of trip in (Lokasimu)!</h3>
                        {/* </br> */}
                        <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                        {/* </br> */}
                        <p>
                          <img src="img/Group-53.png" alt=""/> &ensp;Sewa Mobil Dengan Supir di Bali 12 Jam
                        </p>
                        <p>
                          <img src="img/Group-53.png" alt=""/> &ensp;Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                        <p>
                          <img src="img/Group-53.png" alt=""/> &ensp;Sewa Mobil Jangka Panjang Bulanan</p>
                        <p>
                          <img src="img/Group-53.png" alt=""/> &ensp;Gratis Antar - Jemput Mobil di Bandara</p>
                        <p>
                          <img src="img/Group-53.png" alt=""/> &ensp;Layanan Airport Transfer / Drop In Out</p>
                    </div>
                </div>
            </div>
      </div>
    </div>
  );
};

export default OurServices;