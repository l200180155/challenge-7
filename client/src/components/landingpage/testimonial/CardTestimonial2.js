import React from "react";
import "./Testimonial.css";

const CardCarousel = () => {
  return (
    <div className="owl-carousel owl-theme owl-loaded">
     <div className="item">
          <div className="content">
              <div className="row">
                  <div className="col-sm-3">
                      <div className="profile-image">
                          <img src="img/img-photo-2.png" alt=""/>
                      </div>
                  </div>
                  <div className="col-sm-9">
                      <div className="testimonial-body">
                          <div className="rate">
                              <img src="img/Rate.png" alt=""/>
                          </div>
                          <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                          <p className="name">Iqbal Ramadhani 22</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  );
};

export default CardCarousel;
