// import { Link } from "react-router-dom";
import React from "react";
import './Jumbotron.css'

const Jumbotron = () => {
  return (
    <div className="container-fluid">
      <div className="wrap-landing-page pt-5">
        <div className="jumbotron bg-transparent">
          <div className="row">
              <div className="col-sm-6">
                  <div className="text-landing-page">
                      <h1 className="drop-shadow">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
                      </h1>
                      <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
                      </p>
                      {/* <Link to="/cars" className="btn btn-success btn-lg" role="button">Mulai Sewa Mobil</Link> */}
                  </div>
              </div>
              <div className="col-sm-6">
                  <img className="image" id="cars" img src="img/img-car.png" alt="Ini Gambar Mobil"/>
              </div>
          </div>
        </div> 
      </div>
    </div>
  );
};

export default Jumbotron;
