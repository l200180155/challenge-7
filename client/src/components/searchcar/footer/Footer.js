import React from "react";
import './Footer.css'

const Footer = () => {
  return (
    <div className="container-fluid">
        <div className="margin-content">
            <div className="footer justify-content-between">
                <div className="row">
                    <div className="col-md-3">
                        <div className="contact">
                            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                            <p>binarcarrental@gmail.com</p>
                            <p>081-233-334-808</p>
                        </div>
                    </div>
                    <div className="col-md-2">
                        <div className="link-page">
                            <p><a href="#">Our Services</a></p>
                            <p><a href="#">Why Us</a></p>
                            <p><a href="#">Testimonial</a></p>
                            <p><a href="#">FAQ</a></p>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="social-media">
                            <p>Connect with us</p>
                            <a href="#">
                                <img src="img/icon-facebook.png" alt="" />
                            </a>
                            <a href="#">
                                <img src="img/icon-instagram.png" alt="" />
                            </a>
                            <a href="#">
                                <img src="img/icon-twitter.png" alt="" />
                            </a>
                            <a href="#">
                                <img src="img/icon-mail.png" alt="" />
                            </a>
                            <a href="#">
                                <img src="img/icon-twitch.png" alt="" />
                            </a>
                        </div>
                    </div>
                    <div className="col-md-2">
                        <p className="copyright">Copyright Binar 2022</p>
                        <p className="footer-brand text-decoration: none;"><a href="#">Iqbal</a></p>
                    </div>
                </div>
            </div>
        </div>   
    </div>
  );
};

export default Footer;